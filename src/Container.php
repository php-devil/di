<?php
declare(strict_types=1);

namespace PHPDevil\DI;

use PHPDevil\Di\Contract\DefinitionInterface;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
final class Container extends \DI\Container
{
    public function get(string $id): mixed
    {
        $entity = parent::get($id);
        return $entity instanceof DefinitionInterface ? $entity->resolve($this) : $entity;
    }
}