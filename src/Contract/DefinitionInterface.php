<?php
declare(strict_types=1);

namespace PHPDevil\DI\Contract;

use Psr\Container\ContainerInterface;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
interface DefinitionInterface
{
    public function resolve(ContainerInterface $container): mixed;
}