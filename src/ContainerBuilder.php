<?php
declare(strict_types=1);

namespace PHPDevil\DI;


use Psr\Container\ContainerInterface;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
final class ContainerBuilder extends \DI\ContainerBuilder
{
    public function __construct(string $containerClass = Container::class)
    {
        parent::__construct($containerClass);
    }

    public function load(string|array $paths): self
    {
        if (is_string($paths)) $paths = [$paths];
        foreach ($paths as $path) if (file_exists($path)) {
            if (is_file($path)) $this->addDefinitions($path);
            else foreach (glob($path . '/*.php') as $filename) {
                $this->addDefinitions($filename);
            }
        }
        return $this;
    }

    /**
     * @throws Exception
     */
    public function build(array $setAfterBuild = []): ContainerInterface
    {
        $container = parent::build();
        foreach ($setAfterBuild as $key => $value) {
            $container->set($key, $value);
        }
        return $container;
    }
}