<?php
declare(strict_types=1);

namespace PHPDevil\DI\Definitions;

use PHPDevil\DI\Contract\DefinitionInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
final class Reference implements DefinitionInterface
{
    public function __construct(
        private readonly string $value
    ) {}

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function resolve(ContainerInterface $container): mixed
    {
        return $container->get($this->value);
    }

    public static function to(string $value): self
    {
        return new self($value);
    }
}